/**
 * @author James Lee
 * @version 1.0.0
 * @since 2017-11-09 22:01
 * @description 
 */
var mysql = require('mysql');
/**
 * 여의도티비 서버 sql config -> 실제  db config값으로 바꾸어야 함.
 */
exports.yeouidotvDBconfig = mysql.createPool({
    host     : '182.162.91.135', //182.162.91.135
    user     : 'jh_user',//
    password : 'dudmlehxlql',//
    database : 'sh_application',
    debug    :  false
});

exports.IMODBconfig = mysql.createPool({
    host     : '182.162.91.135', //182.162.91.135
    user     : 'imo_service',//
    password : 'dkdldpadh..!#@',//
    database : 'imo_service',
    debug    :  false
});

/////////////////////////////
/////////TEST CONFIG/////////
/////////////////////////////
/**
 * local sql config(여의도 5층 이종현 과장 자리)
 */
exports.localDBconfig = mysql.createPool({
    // connectionLimit : 1000, 
    host     : 'localhost',
    user     : 'root',
    password : '1234qwer!@#$',
    database : 'yeouidotv',
    debug    :  false
});

/**
 * 여의도티비 테스트서버 sql config(여의도 티비)
 */
exports.yeouidotvTestDBconfig = mysql.createPool({
    host     : '182.162.91.135', //182.162.91.135
    user     : 'jh_user',//
    password : 'dudmlehxlql',//
    database : 'sh_application',
    debug    :  false
});

exports.IMOTestDBconfig = mysql.createPool({
    host     : '182.162.91.135', //182.162.91.135
    user     : 'jh_user',//
    password : 'dudmlehxlql',//
    database : 'sh_application',
    debug    :  false
});
