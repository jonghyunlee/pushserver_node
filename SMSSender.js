var ip = require('ip');
var dateformat = require('dateformat');

var Log = require('./Log');
var Dao = require('./Dao').Dao;
var IMODao = require('./IMODao').IMODao;
var Constants = require('./Constants');
function SMSSender() {
    
}

SMSSender.prototype.smsSender = function (pushResponse, pushSend, pushDeviceInfo) {
    var dateNow = dateformat(Date.now(), "yyyy-mm-dd HH:MM:ss");
    Dao.prototype.setDBData(Constants.DB_SMS_SEND, [pushResponse.seq]);
    /**
    * SMS, LMS 구분하는 코드 들어가야함 SMS기준 80byte 이내
    */
    var mode_text = "LMS";
    if(mode_text == 'SMS') {//(phone, callback, reqdate, msg)
        IMODao.prototype.setDBData(Constants.DB_IMO_SMS, [pushDeviceInfo.g5_mb_hp, 1, dateNow, pushSend.content]);
        
    } else if(mode_text == 'LMS') {//(subject, phone, callback, status, reqdate, msg)
        IMODao.prototype.setDBData(Constants.DB_IMO_MMS, [Constants.YEOUIDOTV_COMPANY, pushDeviceInfo.g5_mb_hp, Constants.YEOUIDOTV_TEL_NUM, 0, dateNow, pushSend.content]);
        
    } else {
        
    }
    //send_id, mode, send_content, send_time, send_realtile, send_ip, state
    Dao.prototype.setDBData(Constants.DB_SMS5_LOG_NEW, [pushSend.title, 0, mode_text, pushSend.content, dateNow, '', ip.address(), '성공']);
    Log.d(mode_text + ' Send');
    
}

module.exports.SMSSender = SMSSender;