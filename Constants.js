/**
 * @author James Lee
 * @version 1.0.0
 * @since 2017-11-09 22:01
 * @description 
 */
/**
 * FCM 전송을 위한 여의도티비 서버 키값
 */
exports.YEOUIDOTV_FCM_KEY = 'AAAAVBY0CIg:APA91bF_jjrN3hbK88N1mWvxI9yZXN_HbED9plm7FvbIaRzFuJS08G8_z_ZpUff7LkgR0bHwaF0xBAFoqa4VzBrOpfBz8WdSEbd_rDJXWErx1a4Vj8v9Rf4thi5LWCin2bA2K0M1F7-2';
/**
 * FCM 전송을 위한 DB 보낼 대상 리스트 조회(SELECT)
 */
exports.DB_DEVICE_LIST = 'DB_DEVICE_LIST'
/**
 * FCM 전송을 위한 DB 보낼 문자내용 조회(SELECT)
 */
exports.DB_PUSH_SEND = 'DB_PUSH_SEND';
/**
 * FCM 전송을 위한 DB TOKEN 값 조회(SELECT)
 */
exports.DB_TOKEN_ID = 'DB_TOKEN_ID';
/**
 * FCM 전송 실패 -> DB 업데이트(UPDATE)
 */
exports.DB_FAIL_FCM_UPDATE = 'DB_FAIL_FCM_UPDATE';
/**
 * FCM 전송 실패->문자전송을 위하여 DB삭제(DELETE)
 */
exports.DB_FAIL_FCM_DELETE = 'DB_FAIL_FCM_DELETE';
/**
 * FCM 전송 후 콜백 성공 받음 -> DB 업데이트(UPDATE)
 */
exports.DB_SUCCESS_FCM = 'DB_SUCCESS_FCM';
/**
 * FCM 전송함 성공 실패여부 모름 -> DB 업데이트(UPDATE)
 */
exports.DB_FCM_SEND = 'DB_FCM_SEND';
/**
 * SMS 전송함 성공 실패여부 모름 -> DB 업데이트(UPDATE)
 */
exports.DB_SMS_SEND = 'DB_SMS_SEND';
/**
 * SMS 전송 시 로그파일 작성
 */
exports.DB_SMS5_LOG_NEW = 'DB_SMS5_LOG_NEW';
/**
 * IMO SMS insert
 */
exports.DB_IMO_SMS = 'DB_IMO_SMS';
/**
 * IMO MMS insert
 */
exports.DB_IMO_MMS = 'DB_IMO_MMS';
/**
 * 새로운 DB가 쌓일때까지 기다리는 시간(Push_DeviceInfo)
 */
exports.DEFAULT_DB_WAIT_TIME = 20 * 1000;// 20sec
/**
 * DB connection을 계속 물고있을수 없기때문에 timeout을 걸어준다
 */
exports.DEFAULT_DB_TIMEOUT = 10000;// 10sec
/**
 * 로그를 찍을시에 +9시간이 되기때문에 그것을 방지하기 위함
 */
exports.NINE_HOUR = 1000 * 60 * 60 * 9;// 9hour
/**
 * 여의도티비 대표번호 for sms
 */
exports.YEOUIDOTV_TEL_NUM = 16002391;
/**
 * 
 */
exports.YEOUIDOTV_COMPANY = '[여의도티비]';