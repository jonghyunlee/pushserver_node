
/**
 * Program Version : 1.0.0
 * Author : JamesLee (leejh8848@ysmg.co.kr, 010-5149-8848)
 * 주요기능 : 푸시메시지 전송, 데이터베이스 변경, SMS 전송(예정)
 * 각각의 스크립트, 디렉토리 간략 설명 :
 *      log - 콘솔에 찍히는 로그내용들 파일로 작성하여 저장되는 디렉토리
 *      node_modules - 프로그램 내에서 사용하는 외장 라이브러리 집합체
 *      configs.js - DB 접근정보를 담고있는 스크립트
 *      Constants.js - 상수들을 모아놓은 스크립트
 *      Dao.js - DB selelct, update, delete 등등의 DB작업을 담당하는 스크립트
 *      index.js - FCM전송을 담당하는메인 스크립트(메인함수 같은 것)
 *      Log.js - 콘솔에 로그내용을 뿌려주고 일별(09시 초기화)로 파일로 작성하도록 해주는 스크립트
 *      README.js - 인수인계 파일
 * 
 * 1. 프로그램이 멈춤
 *      vscode 콘솔창에 node index 입력
 * 
 * 2. 데이터베이스 서버 접근정보 변경
 *      configs.js -> yeouidotvDBconfig, IMODBconfig 변수의 접근정보 변경
 *      Dao.js, IMODao.js -> pool = configs.yeouidotvDBconfig, configs.IMODBconfig;변수 확인
 * 
 * 3. DB Watching 간격 조정
 *      Constants.js -> DEFAULT_DB_TIMEOUT 값 변경 (ms단위)
 * 
 * 4. 프로그램 종료
 *      vscode 콘솔창에 control + c;
 * 
 * 5. 프로그램 시작
 *      프로그램 path(/Users/jameslee/Desktop/이종현/dev/pushServer)로 이동
 *      vscode 콘솔창에 'node index.js' 입력
 * 
 * 6. 로그데이터 확인
 *      프로그램 로그 path(/Users/jameslee/Desktop/이종현/dev/pushServer/log)로 이동
 *      일별 로그데이터 확인(매일 09시 초기화)
 * 
 * 7. 퍼포먼스 테스트 결과(2017-11-13 13:00기준)
 *      1000개 전송 평균 12s 500ms
 *      5000/min
 */