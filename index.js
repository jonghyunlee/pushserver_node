/**
* @author James Lee
* @version 1.0.0
* @since 2017-11-09 22:01
* @description 
*/
var FCM = require('fcm-node');
var mysql = require('mysql');
var async = require('async');
var forEach = require('async-foreach').forEach;
var dateformat = require('dateformat');

var Constants = require('./Constants');
var Log = require('./Log');
var configs = require('./configs');
var Dao = require('./Dao').Dao;
var SMSSender = require('./SMSSender').SMSSender;
var fcm = new FCM(Constants.YEOUIDOTV_FCM_KEY);

/**
* 마지막에 전송된 Push_Response seq 데이터.(커서 개념)
*/
var last_seq = 0;
/**
* 프로그램 시작 후 전송한 푸시메시지의 총 개수.
*/
var send_cnt = 0;
/**
* 프로그램 시작 후 발생한 fcm오류 총 개수.
*/
var error_cnt = 0;
/**
* 프로그램 시작 시간
*/
var program_startTime = Date.now();
/**
* 프로세스 시작 시간
*/
var process_startTime;
/**
* 프로세스 종료 시간
*/
var process_endTime;

/**
*  waterfall model을 이용하여 기본적인 시퀀스는 동기처리로 진행하였고, 세부적인(db update, db delete, fcm send...)내용은 비동기로 처리하였음.
*/
var startPushService = function () {
    process_startTime = Date.now();
    Log.d('start PushService process last_seq : ' + last_seq);
    async.waterfall(waterfallTask, null);
    
};


/**
 * 총 3개의 단위로 구성되어있으며
 * 1st func : 푸시메시지를 보낼 리스트를 가져옴
 * 2st func : 가져온 리스트를 기반으로 개별 데이터(title, body, token, seq등등)을 가져와 푸시메시지를 전송 (메인 로직)
 * 3st func : 하나의 프로세스가 종료되면 로그를남기고 바로 다음 프로세스 진행
 */
var waterfallTask = [
    function(callback){//1st func
        Dao.prototype.getDBData(Constants.DB_DEVICE_LIST, [last_seq], function (err, rows) {
            if(err){
                Log.d('DB_DEVICE_LIST error uccur' + err);
                
            }
            if(rows){//db에 데이터가 있을경우 다음 로직을 진행시킴
                //TODO if you want loop test, write code this row using last_seq
                callback(null, rows);
            } else {//db에 데이터가 없을경우 Constants.DEFAULT_DB_WAIT_TIME만큼 대기
                Log.d('not exsist DB_DEVICE_LIST DB....wait '+Constants.DEFAULT_DB_WAIT_TIME/1000+' sec...send_cnt : ' + send_cnt + '...error_cnt : ' + error_cnt);
                setTimeout(startPushService, Constants.DEFAULT_DB_WAIT_TIME);//보낼 푸시메시지가 없을경우 Constants.DEFAULT_DB_WAIT_TIME만큼 대기하고 재검색을 시작한다.
                
            }
            
        })
    }, function(rows, callback) {//2st func
        //1st func에서 가져온 리스트를 기반으로 개별 데이터(title, body, token, seq등등)을 가져와 푸시메시지를 forEach문을 이용하여 전송 (메인 로직)
        async.forEachLimit(rows, rows.length, function (pushResponse, maincallback) {
            /**
             * 총 3개의 단위로 구성되어있으며
             * 2-1st func : FCM 메시지를 보낼 내용(title, body)를 가져옴
             * 2-2st func : FCM 메시지를 보낼 FCM ID(token) 가져옴
             * 2-3st func : FCM 메시지 전송 및 전송결과 DB 업데이트
            */
            async.waterfall([
                function (callback) {//2-1st func
                    Dao.prototype.getDBData(Constants.DB_PUSH_SEND, [pushResponse.send_seq], function (err, rows) {
                        var title, body;
                        if(err){
                            Log.d('DB_PUSH_SEND error uccur' + err);
                            return;
                        }
                        if(rows){
                            title = rows[0].title;
                            body = rows[0].content;
                        }
                        callback(null, pushResponse, rows[0]);
                        
                    });
                    
                }, function (pushResponse, pushSend, callback) {//2-2st func
                    Dao.prototype.getDBData(Constants.DB_TOKEN_ID, [pushResponse.g5_mb_id], function (err, rows) {
                        var to;
                        if(err){
                            Log.d('DB_TOKEN_ID error uccur' + err);
                            return;
                        }
                        if(rows){
                            to = rows[0].push_id;
                            callback(null, pushResponse, rows[0], pushSend);
                            
                        }
                        
                    });
                }, function (pushResponse, pushDeviceInfo, pushSend, callback) {//2-3st func
                    var message = { 
                        to: pushDeviceInfo.push_id, 
                        priority: 'high',
                        data: {
                            title: pushSend.title,
                            body: pushSend.content
                        }
                    }
                    Dao.prototype.setDBData(Constants.DB_FCM_SEND, [pushResponse.seq]);
                    fcm.send(message, function (err, response) {//FCM 전송
                        send_cnt = send_cnt + 1;
                        
                        if(err){
                            var fcm_error;
                            Log.d('FCM ERROR : '+ send_cnt + '...error_cnt : ' + error_cnt + '...' + err + '...pushResponse.g5_mb_id : ' + pushResponse.g5_mb_id + '...pushResponse.seq : ' + pushResponse.seq);
                            error_cnt =  error_cnt + 1;
                            try {
                                fcm_error = JSON.parse(err).results[0].error;
                                
                            } catch (error) {
                                fcm_error = err.toString();
                            }
                            Dao.prototype.setDBData(Constants.DB_FAIL_FCM_UPDATE, [fcm_error, pushResponse.seq]);
                            Dao.prototype.setDBData(Constants.DB_FAIL_FCM_DELETE, [pushResponse.seq]);
                            
                            //푸시전송 실패 시 SMS를 전송한다.(코드작성 필요)
                            SMSSender.prototype.smsSender(pushResponse, pushSend, pushDeviceInfo);
                            
                        } else {
                            Dao.prototype.setDBData(Constants.DB_SUCCESS_FCM, [pushResponse.seq]);
                        }
                    });
                }
            ]);
        });
        callback(null, rows);
        //하나의 프로세스가 완료되면 로그를 남기고 다음 프로세스를 시작한다.(재귀함수 형식)
    }, function (rows) {//3st func
        last_seq = rows[rows.length-1].seq;
        process_endTime = Date.now();
        Log.d('end last_seq :' + last_seq + '...process interval : ' + dateformat(process_endTime-process_startTime-Constants.NINE_HOUR, "HH:MM:ss l"));
        Log.d('process done ...program_startTime : ' + dateformat(program_startTime, "yyyy-mm-dd HH:MM:ss l") + '...process_startTime : ' + dateformat(process_startTime, "yyyy-mm-dd HH:MM:ss l") + '...process_endTime : ' + dateformat(process_endTime, "yyyy-mm-dd HH:MM:ss l") + '...program runningtime : ' + dateformat(process_endTime-program_startTime-Constants.NINE_HOUR, "yyyy-mm-dd HH:MM:ss l") + '...send_cnt : ' + send_cnt + '...error_cnt : ' + error_cnt);
        
        startPushService();//푸시메시지전송 프로세스를 마치면, DB를 재검색하여 푸시를 전송한다.
    }
];


startPushService();