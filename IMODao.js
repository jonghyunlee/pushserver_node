var configs = require('./configs');
var mysql = require('mysql');
var Log = require('./Log');
var Constants = require('./Constants');

var pool = configs.IMOTestDBconfig;
function IMODao() {
    
}
/**
* 
* @param {*} mode 
* @param {*} values 각각의 쿼리에 들어갈 데이터들
*/
IMODao.prototype.setDBData = function(mode, values) {
    // Log.d('setDBData start : ');
    
    pool.getConnection(function (err, conn) {
        // Log.d('setDBData getConnection start : ');
        var query;
        if(err){
            if(conn){
                conn.release();
            }   
            Log.d('setDBData : ' + err);
            return;
        };
        //switch 문을 이용하여 mode에 맞는 쿼리 작성
        switch (mode) {
            case Constants.DB_IMO_SMS:
            query = "insert into SMS_MSG(phone, callback, reqdate, msg) values (?, ?, ?, ?)";
            break;
            
            case Constants.DB_IMO_MMS:
            query = "insert into MMS_MSG(subject, phone, callback, status, reqdate, msg) values (?, ?, ?, ?, ?, ?)";
            break;
            
            default:
            break;
        }
        
        var exec = conn.query({
            sql: query,
            timeout: Constants.DEFAULT_DB_TIMEOUT,
            values: values
        }, function (err, rows) {
            conn.release();
            if(err){
                Log.d('IMO setData query error : ' + err);
                Log.d('IMO setData query: ' + query + '..values :' + values);
                return;
            }
        });
        
    });
    
}

module.exports.IMODao = IMODao;