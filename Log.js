/**
 * @author James Lee
 * @version 1.0.0
 * @since 2017-11-09 22:01
 * @description 
 * 
 */
var winston = require('winston');
var winstonDaily = require('winston-daily-rotate-file');
var moment = require('moment');

/**
 * 로그데이터를 일마다 파일로 저장하기 위한 변수(초기화 시간 매일 09:00)
 */
var logger = new (winston.Logger)({
    transports: [
        new (winstonDaily)({
            name: 'debug-file',
            filename: './log/debug',
            datePattern: '_yyyy-MM-dd.log',
            colorize: true,
            maxsize: 10000000,
            maxFile: 1000,
            level: 'debug',
            showLevel: true,
            json: false,
            createTree:true,
            timestamp: timeStampFormat
        }),
        new(winston.transports.Console)({
            name:'debug-console',
            colorize: true,
            level: 'debug',
            showLevel: true,
            json: false,
            timestamp:timeStampFormat
        }),
        new (winstonDaily)({
            name: 'info-file',
            filename: './log/info',
            datePattern: '_yyyy-MM-dd.log',
            colorize: true,
            maxsize: 10000000,
            maxFile: 1000,
            level: 'info',
            showLevel: true,
            json: false,
            createTree:true,
            timestamp: timeStampFormat
        }),
        new(winston.transports.Console)({
            name:'info-console',
            colorize: true,
            level: 'info',
            showLevel: true,
            json: false,
            timestamp:timeStampFormat
        })
    ],
    exceptionHandlers:[
        new(winstonDaily)({
            name: 'exception-file',
            filename: './log/exception',
            datePattern: '_yyyy-MM-dd.log',
            colorize: false,
            maxsize: 5000000,
            maxFile: 1000,
            level: 'error',
            showLevel: true,
            json: false,
            createTree:true,
            timestamp:timeStampFormat
        }),
        new(winston.transports.Console)({
            name:'exception-console',
            colorize: true,
            level: 'debug',
            showLevel: true,
            json: false,
            timestamp:timeStampFormat
        })
    ]
})

exports.v = function (message) {
};

exports.d = function (message) {
    logger.debug(message);
};

exports.i = function (message) {
    
};
exports.w = function (message) {
    
};


function timeStampFormat() {
    return moment().format('YYYY-MM-DD HH:mm:ss.SSS');
}