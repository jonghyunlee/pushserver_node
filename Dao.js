
var configs = require('./configs');
var mysql = require('mysql');
var Log = require('./Log');
var Constants = require('./Constants');
var pool = configs.yeouidotvDBconfig;;
function Dao() {
    
}
/**
* 
* @param {*} mode 
*              Constants.DB_DEVICE_LIST : 푸시메시지를 보낼 리스트를 가져옴, 
*              Constants.DB_PUSH_SEND : 푸시메시지를 보낼 내용을 가져옴, 
*              Constants.DB_TOKEN_ID 푸시메시지를 보낼 대상을 가져옴
* @param {*} values 각각의 쿼리에 들어갈 데이터들(seq, seq, g5_mb_id)
* @param {*} callback 
*/
Dao.prototype.getDBData = function(mode, values, callback) {
    pool.getConnection(function (err, conn) {
        var query;
        if(err){
            if(conn){
                conn.release();
            }   
            Log.d('getDBData : ' + err);
            callback(err, null);
            return;
        };
        //switch 문을 이용하여 mode에 맞는 쿼리 작성
        switch (mode) {
            case Constants.DB_DEVICE_LIST:
            query = "select * from Push_Response where is_send = 'N' AND  seq > ? order by seq asc limit 1000";
            // query = "select * from Push_Response where seq > ? order by seq asc limit 1000";
            break;
            
            case Constants.DB_PUSH_SEND:
            query = "select * from Push_Send where seq =?";
            break;
            
            case Constants.DB_TOKEN_ID:
            query = "select * from Push_DeviceInfo where g5_mb_id = ?";
            break;
            
            default:
            callback(null, null);
            break;
        }
        //쿼리 실행후 실행결과 callback
        var exec = conn.query({
            sql: query,
            timeout: Constants.DEFAULT_DB_TIMEOUT,
            values: values
        }, function (err, rows) {
            conn.release();
            if(err){
                Log.d('getData query error : ' + err);
                Log.d('getData query : ' + query + '..values :' + values);
                callback(err, null);
                return;
            }
            
            if(rows != null && rows.length > 0){ //exsist DB
                callback(null, rows);
                // Log.d('get rows : ' + query + '...data : ' + data);
            } else { //not exsist DB
                callback(null, null);
            }
            
        });
    });
    // Log.d('getDBData end : ');
    
}
/**
* 
* @param {*} mode 
*              Constants.DB_FAIL_FCM_UPDATE : FCM전송 실패시 Push_Response memo값 업데이트
*              Constants.DB_FAIL_FCM_DELETE : FCM전송 실패시 Push_DeviceInfo값 삭제
*              Constants.DB_FCM_SEND : FCM전송시 Push_Response is_send, memo값 업데이트
*              Constants.DB_SUCCESS_FCM : FCM전송 성공시 Push_Response is_receive, receive_datetime값 업데이트
* @param {*} values 각각의 쿼리에 들어갈 데이터들
*/
Dao.prototype.setDBData = function(mode, values) {
    // Log.d('setDBData start : ');
    
    pool.getConnection(function (err, conn) {
        // Log.d('setDBData getConnection start : ');
        var query;
        if(err){
            if(conn){
                conn.release();
            }   
            Log.d('setDBData : ' + err);
            return;
        };
        //switch 문을 이용하여 mode에 맞는 쿼리 작성
        switch (mode) {
            case Constants.DB_FAIL_FCM_UPDATE:
            query = "UPDATE Push_Response SET memo = CONCAT(memo, ?) Where seq = ?";
            break;
            
            case Constants.DB_FAIL_FCM_DELETE:
            query = "DELETE FROM Push_DeviceInfo WHERE  seq in (select PDI_seq from Push_Response where seq = ?)";
            break;
            
            case Constants.DB_FCM_SEND:
            query = "UPDATE Push_Response SET is_send = 'Y', Push_datetime = now(), memo = 'push send \n' Where seq =?";
            break;
            
            case Constants.DB_SUCCESS_FCM:
            query = "UPDATE Push_Response SET is_receive = 'Y', receive_datetime = now() Where seq = ?";
            break;
            
            case  Constants.DB_SMS_SEND:
            query = "UPDATE Push_Response SET is_sms = 'Y', `memo` = CONCAT(memo, ' \n sms send \n')  Where seq = ? AND is_sms = 'N'";

            break;
            case Constants.DB_SMS5_LOG_NEW:
            query = "insert into sms5_log_new (send_id, mode, send_content, send_time, send_realtime, send_ip, state) values (?, ?, ?, ?, ?, ?, ?)";
            default:
            break;
        }
        
        var exec = conn.query({
            sql: query,
            timeout: Constants.DEFAULT_DB_TIMEOUT,
            values: values
        }, function (err, rows) {
            conn.release();
            if(err){
                Log.d('1.setData query error : ' + err);
                Log.d('1.setData query: ' + query + '..values :' + values);
                return;
            }
            
        });
        
    });
    
}

module.exports.Dao = Dao;